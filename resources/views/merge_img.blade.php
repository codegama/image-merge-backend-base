@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Upload Image</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('upload-img') }}" enctype="multipart/form-data">
                        @csrf

                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">Image 1</label>

                            <div class="col-md-6">
                                <input id="img_1" type="file" class="form-control" name="img_1" value="" required>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">Image 2</label>

                            <div class="col-md-6">
                                <input id="img_2" type="file" class="form-control" name="img_2" value="" required>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">Image 3</label>

                            <div class="col-md-6">
                                <input id="img_3" type="file" class="form-control" name="img_3" value="">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">Image 4</label>

                            <div class="col-md-6">
                                <input id="img_4" type="file" class="form-control" name="img_4" value="">
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-8 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    Submit
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
