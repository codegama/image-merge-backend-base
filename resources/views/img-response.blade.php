@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Output Image</div>

                <div class="card-body">
                    <div style="text-align: center;">
                        <div>
                            <img style="width:50%" src="{{$img_output}}">
                        </div>
                        <div>
                            <a style="background: #3490dc;color: white;padding: 5px 10px;border-radius: 6px;margin-right: 6px;" href="{{$img_output}}" download>
                                Download Image
                            </a>

                            <a style="background: #3490dc;color: white;padding: 5px 10px;border-radius: 6px;margin-right: 6px;" href="/">Upload Again</a>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
