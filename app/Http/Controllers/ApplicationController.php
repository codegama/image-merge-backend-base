<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Log, Validator, Exception, DB, Setting, Storage;

class ApplicationController extends Controller
{

    public function upload_img(Request $request) {

        try {

            $x = $y = 600;

            $outputImage = imagecreatetruecolor(600, 600);

            $white = imagecolorallocate($outputImage, 255, 255, 255);

            imagefill($outputImage, 0, 0, $white);

            if ($request->hasFile('img_1')) {
                
                $ext1 = $request->img_1->getClientOriginalExtension();

                $file_name = rand().".".$ext1;

                Storage::putFileAs("public/", $request->img_1, $file_name);

                $storage_file_path = $file_name;

                $url_img1 = asset(Storage::url($storage_file_path));

                $img1 = "storage/".$storage_file_path;

                $first = imagecreatefrompng($img1);

                imagecopyresized($outputImage,$first,0,0,0,0, $x, $y,$x,$y);
            }

            if ($request->hasFile('img_2')) {
                
                $ext2 = $request->img_2->getClientOriginalExtension();

                $file_name2 = rand().".".$ext2;

                Storage::putFileAs("public/", $request->img_2, $file_name2);

                $storage_file_path2 = $file_name2;

                $url_img2 = asset(Storage::url($storage_file_path2));

                $img2 = "storage/".$storage_file_path2;

                $second = imagecreatefrompng($img2);

                imagecopyresized($outputImage,$second,0,0,0,0, $x, $y,$x,$y);
            }

            if ($request->hasFile('img_3')) {
                
                $ext3 = $request->img_3->getClientOriginalExtension();

                $file_name3 = rand().".".$ext3;

                Storage::putFileAs("public/", $request->img_3, $file_name3);

                $storage_file_path3 = $file_name3;

                $url_img3 = asset(Storage::url($storage_file_path3));

                $img3 = "storage/".$storage_file_path3;

                $third = imagecreatefrompng($img3);

                imagecopyresized($outputImage,$third,0,0,0,0, $x, $y,$x,$y);
            }

            if ($request->hasFile('img_4')) {
                
                $ext4 = $request->img_4->getClientOriginalExtension();

                $file_name4 = rand().".".$ext4;

                Storage::putFileAs("public/", $request->img_4, $file_name4);

                $storage_file_path4 = $file_name4;

                $url_img4 = asset(Storage::url($storage_file_path4));

                $img4 = "storage/".$storage_file_path4;

                $four = imagecreatefrompng($img4);

                imagecopyresized($outputImage,$four,0,0,0,0, $x, $y,$x,$y);
            }

            $filename ="storage/" .round(microtime(true)).'.png';

            imagepng($outputImage, $filename);
            
            $img_output = asset($filename) ?? asset('placeholder.jpeg');

            return view('img-response')
                ->with('img_output', $img_output);

        } catch(Exception $e) {

            $response_array = ['success' => false,'error' => $e->getMessage(),'error_code' => 101];

            return response()->json($response_array , 200);

        }
   
    }


}
